<?php
// Test CVS

require_once 'Excel/reader.php';


// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();


// Set output Encoding.
$data->setOutputEncoding('utf-8');

/***
* 如果你想使用mb_convert_encoding来改变字符编码:
* $data->setUTFEncoder('mb');
*
**/

/***
 * 默认情况下，行和列从1开始
* 更改初始索引可以使用:
* $data->setRowColOffset(0);
*
**/



/***
*  格式化输出的一些函数.
* $data->setDefaultFormat('%.2f');
* setDefaultFormat - 设置格式未知的列的格式
*
* $data->setColumnFormat(4, '%.3f');
* setColumnFormat - 为列设置格式（仅适用于数字字段）
*
**/

$data->read('xls.xls');

/*


 $data->sheets[0]['numRows'] - 总行数
 $data->sheets[0]['numCols'] - 总列数
 $data->sheets[0]['cells'][$i][$j] - 获取$i行$j列的数据

 $data->sheets[0]['cellsInfo'][$i][$j] - 单元格的伸展信息
    
    $data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
    	如果设为"unknown"，使用未加工的值，所有单元格里面的值的格式为'0.00';
    $data->sheets[0]['cellsInfo'][$i][$j]['raw'] = 单元格无格式时的值
    $data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
    $data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 
*/

error_reporting(E_ALL ^ E_NOTICE);

for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
	for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
		echo "\"".$data->sheets[0]['cells'][$i][$j]."\",";
	}
	echo "<br/>";

}


var_dump($data);
var_dump($data->formatRecords);
?>
